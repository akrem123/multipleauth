module.exports = {
    mongoURI: process.env.MONGO_URI,
    secretOrKey: process.env.SECRET_OR_KEY,
    GOOGLE_MAP_KEY: process.env.GOOGLE_MAP_KEY
  };
  